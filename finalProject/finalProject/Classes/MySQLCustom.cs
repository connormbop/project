﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;

using System.Data;
using MySql.Data.Common;
using MySql.Data.Types;

namespace MySQLDemo.Classes
{
    public class MySQLCustom
    {
        //CHANGE THESE TO MATCH YOUR DB
        private static string host = "localhost";
        private static string user = "root";
        private static string pass = "root";
        private static string dbname = "gui_comp6001_16b_assn3";

        //DO NOT CHANGE THESE
        public static MySqlConnection Connection(string h, string u, string p, string n)
        {
            string serverConnection = String.Format($"Server={h};Database={n};Uid={u};Pwd={p};SslMode=None;charset=utf8");

            MySqlConnection connection = new MySqlConnection(serverConnection);

            return connection;
        }

        public static MySqlConnection conn()
        {
            var a = Connection(host, user, pass, dbname);
            return a;
        }

        public class Results
        {
            public string Row { get; set; }
        }

        public static List<Results> SelectCommand(string command, MySqlConnection connection)
        {
            //Fixes Encoding - Courtsey of Stackoverflow :-)
            EncodingProvider p;
            p = CodePagesEncodingProvider.Instance;
            Encoding.RegisterProvider(p);

            List<Results> listTables = new List<Results>();

            using (connection)
            {
                connection.Open();

                MySqlCommand getCommand = connection.CreateCommand();
                getCommand.CommandText = command;
                using (MySqlDataReader reader = getCommand.ExecuteReader())
                {
                    while(reader.Read())
                    {
                        for (var i = 0; i < reader.FieldCount; i++)
                        {
                            listTables.Add(new Results() { Row = $"{reader.GetValue(i)}"});
                        }
                    }
                }
            }

            return listTables;
        }

        public static List<string> ShowInList(string command)
        {
            var dataList = new List<string>();
            var list = new List<Results>(SelectCommand(command, conn()));

            for (var i = 0; i < list.Count; i ++)
            {
                dataList.Add($"{list[i].Row}");
            }

            return dataList;
        }

        public static string ShowInString(string command)
        {
            var dataList = new List<string>();
            var list = new List<Results>(SelectCommand(command, conn()));

            for (var i = 0; i < list.Count; i ++)
            {
                dataList.Add($"{list[i].Row}");
            }

            return string.Join(",", dataList);
        }


        //////////////////    DB CLASS ///////////////////////////

        //CHANGE THIS!!
        //This class is specific to your DB
        public class AddPerson
        {
            public int id { get; set; }

            public string fname { get; set; }
            public string lname { get; set; }

            public DateTime dob { get; set; }
            public int strNum { get; set; }

            public string strName { get; set; }

            public string pstCode { get; set; }

            public string ctyName { get; set; }

            public string ph1 { get; set; }
            public string ph2 { get; set; }

            public string emailAdd { get; set; }

            public AddPerson(int _id, string _fname, string _lname, DateTime _dob)
            {
                fname = _fname;
                lname = _lname;
                dob = _dob;
                id = _id;
            }

            public AddPerson(int _id)
            {
                id = _id;
            }

            public AddPerson(int _id, string _fname, string _lname, DateTime _dob, int _strNum, string _strName, string _pstCode ,string _ctyName, string _ph1, string _ph2, string _emailAdd)
            {
                fname = _fname;
                lname = _lname;
                strName = _strName;
                ctyName = _ctyName;
                emailAdd = _emailAdd;
                strNum = _strNum;
                pstCode = _pstCode;
                ph1 = _ph1;
                ph2 = _ph2;
                dob = _dob;
                id = _id;
            }
            public AddPerson(string _fname, string _lname, DateTime _dob, int _strNum, string _strName, string _pstCode, string _ctyName, string _ph1, string _ph2, string _emailAdd)
            {
                fname = _fname;
                lname = _lname;
                strName = _strName;
                ctyName = _ctyName;
                emailAdd = _emailAdd;
                strNum = _strNum;
                pstCode = _pstCode;
                ph1 = _ph1;
                ph2 = _ph2;
                dob = _dob;
            }
        }

        ////////////////////  INSERT STATEMENTS ///////////////////

        //CHANGE THIS!!
        //The Insert Statement needs to be customised
        public static void AddData(string fname, string lname, string dob, string strNum, string strName, string postCode, string cityName, string phoneNum1, string phoneNum2, string email)
        {
            var date = DateTime.Parse(dob);
            //var ID = int.Parse(idNum);
            var strNumber = int.Parse(strNum);

            var customdb = new AddPerson(fname, lname, date, strNumber, strName, postCode, cityName, phoneNum1, phoneNum2, email);

            var con = conn();
            con.Open();

            MySqlCommand insertCommand = con.CreateCommand();

            insertCommand.CommandText = "INSERT INTO tbl_people(FNAME, LNAME, DOB, Str_NUMBER, Str_NAME, POSTCODE, CITY, PHONE1, PHONE2, EMAIL)VALUES(@fname, @lname, @dob, @strNum, @strName, @postCode, @cityName, @phoneNum1, @phoneNum2, @email)";
            //insertCommand.Parameters.AddWithValue("@ID", customdb.id);
            insertCommand.Parameters.AddWithValue("@fname", customdb.fname);
            insertCommand.Parameters.AddWithValue("@lname", customdb.lname);
            insertCommand.Parameters.AddWithValue("@dob", customdb.dob);
            insertCommand.Parameters.AddWithValue("@strNum", customdb.strNum);
            insertCommand.Parameters.AddWithValue("@strName", customdb.strName);
            insertCommand.Parameters.AddWithValue("@postCode", customdb.pstCode);
            insertCommand.Parameters.AddWithValue("@cityName", customdb.ctyName);
            insertCommand.Parameters.AddWithValue("@phoneNum1", customdb.ph1);
            insertCommand.Parameters.AddWithValue("@phoneNum2", customdb.ph2);
            insertCommand.Parameters.AddWithValue("@email", customdb.emailAdd);
            insertCommand.ExecuteNonQuery();

            con.Clone();
        }



        ////////////////////  UPDATE STATEMENTS ///////////////////

        //CHANGE THIS!!
        //The Insert Statement needs to be customised
        public static void UpdateData(string idNum, string fname, string lname, string dob, string strNum, string strName, string postCode, string cityName, string phoneNum1, string phoneNum2, string email)
        {
            var date = DateTime.Parse(dob);
            var ID = int.Parse(idNum);
            var strNumber = int.Parse(strNum);

            var customdb = new AddPerson(ID ,fname, lname, date, strNumber, strName, postCode, cityName, phoneNum1, phoneNum2, email);

            var con = conn();
            con.Open();

            MySqlCommand updateCommand = con.CreateCommand();

            //UPDATE TABLE-NAME SET COLUMN = VALUE WHERE COLUMN = VALUE

            updateCommand.CommandText = "UPDATE tbl_people SET FNAME = @fname, LNAME = @lname, DOB = @dob, Str_NUMBER = @strNum, Str_NAME = @strName, POSTCODE = @postCode, CITY = @cityName, PHONE1 = @phoneNum1, PHONE2 = @phoneNum2 , EMAIL = @email WHERE ID = @ID";
            updateCommand.Parameters.AddWithValue("@ID", customdb.id);
            updateCommand.Parameters.AddWithValue("@fname", customdb.fname);
            updateCommand.Parameters.AddWithValue("@lname", customdb.lname);
            updateCommand.Parameters.AddWithValue("@dob", customdb.dob);
            updateCommand.Parameters.AddWithValue("@strNum", customdb.strNum);
            updateCommand.Parameters.AddWithValue("@strName", customdb.strName);
            updateCommand.Parameters.AddWithValue("@postCode", customdb.pstCode);
            updateCommand.Parameters.AddWithValue("@cityName", customdb.ctyName);
            updateCommand.Parameters.AddWithValue("@phoneNum1", customdb.ph1);
            updateCommand.Parameters.AddWithValue("@phoneNum2", customdb.ph2);
            updateCommand.Parameters.AddWithValue("@email", customdb.emailAdd);
            updateCommand.ExecuteNonQuery();

            con.Clone();
        }

        ////////////////////  DELETE STATEMENTS ///////////////////

        //CHANGE THIS!!
        //The Insert Statement needs to be customised
        public static void DeleteDate(string idNum)
        {
            var ID = int.Parse(idNum);

            var customdb = new AddPerson(ID);

            var con = conn();
            con.Open();

            MySqlCommand updateCommand = con.CreateCommand();

            //DELETE FROM TABLE-NAME WHERE COLUM = VALUE

            updateCommand.CommandText = "DELETE FROM tbl_people WHERE ID = @id";
            updateCommand.Parameters.AddWithValue("@id", customdb.id);
            updateCommand.ExecuteNonQuery();

            con.Clone();
        }
    }
}
