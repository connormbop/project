﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Drawing;
using System.Windows.Input;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Popups;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using MySql.Data.MySqlClient;
using MySQLDemo.Classes;
// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace finalProject
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public MainPage()
        {
            this.InitializeComponent();
            Welcome();
        }
        private async void Welcome()
        {
            MessageDialog text = new MessageDialog($"\n\t Welcome to my Contact form");
            text.Title = "Contact Form";
            
            text.Commands.Add(new Windows.UI.Popups.UICommand($"Continue") { Id = 0 });

            text.DefaultCommandIndex = 0;

            var output = await text.ShowAsync();
        }
        private void SubmitButton_Click(object sender, RoutedEventArgs e)
        {

        }

        private void HamburgerButton_Click(object sender, RoutedEventArgs e)
        {
            MySplitView.IsPaneOpen = !MySplitView.IsPaneOpen;
            MySplitView.Background = new SolidColorBrush(Windows.UI.Color.FromArgb(0, 119, 136, 153));
        }

        private void Data_Click(object sender, RoutedEventArgs e)
        {
            pivot1pane.Background = new SolidColorBrush(Windows.UI.Color.FromArgb(255, 255, 255, 255));
            pivot2Pane.Background = new SolidColorBrush(Windows.UI.Color.FromArgb(255, 255, 255, 255));
            Pivot.SetValue(Grid.ColumnSpanProperty, 3);
            Pivot.SetValue(Grid.RowSpanProperty, 6);
            Pivot.Margin = new Thickness(0,40,0,0);
            listOverlay.ClearValue(ListView.ItemsSourceProperty);
            currentEntry.Text = "";

            pivot2Pane.ItemsSource = loadAllTheNumbers();
        }

        private void updateBtn_Click(object sender, RoutedEventArgs e)
        {
            MySQLCustom.UpdateData(idNum.Text,fName.Text, lName.Text, DOB.Text, strNum.Text, strName.Text, postCode.Text, cityName.Text, phoneNum1.Text, phoneNum2.Text, email.Text);
        }

        private void removeBtn_Click(object sender, RoutedEventArgs e)
        {
            MySQLCustom.DeleteDate(idNum.Text);
        }

        private void addBtn_Click(object sender, RoutedEventArgs e)
        {
            MySQLCustom.AddData(fName.Text, lName.Text, DOB.Text, strNum.Text, strName.Text, postCode.Text, cityName.Text, phoneNum1.Text, phoneNum2.Text, email.Text);
        }
        
        private void Home_Click(object sender, RoutedEventArgs e)
        {
            pivot1pane.Background = new SolidColorBrush(Windows.UI.Color.FromArgb(0, 0, 0, 0));
            Pivot.SetValue(Grid.ColumnSpanProperty, 1);
            Pivot.SetValue(Grid.RowSpanProperty, 1);
            Pivot.Margin = new Thickness(0, 200, 0, 0);


        }

        private void searchBtn_Click(object sender, RoutedEventArgs e)
        {
            pivot2Pane.ClearValue(ListView.ItemsSourceProperty);

            var command = $"Select * from tbl_people WHERE ID = '{searchDB.Text}' ";
            var b = MySQLCustom.ShowInList(command);
            FirstNameDisplay.Text = b[1];
            LastNameDisplay.Text = b[2];
            DateOfBirthDisplay.Text = b[3];
            strNumDisplay.Text = b[4];
            strNameDisplay.Text = b[5];
            postCodeDisplay.Text = b[6];
            cityNameDisplay.Text = b[7];
            phoneNum1Display.Text = b[8];
            phoneNum2Display.Text = b[9];
            emailDisplay.Text = b[10];
        }

        private void autoFill_Click(object sender, RoutedEventArgs e)
        {
            var command = $"Select * from tbl_people WHERE ID = '{idNum.Text}' ";
            var b = MySQLCustom.ShowInList(command);

            fName.Text = b[1];
            lName.Text = b[2];
            DOB.Text = b[3];
            strNum.Text = b[4];
            strName.Text = b[5];
            postCode.Text = b[6];
            cityName.Text = b[7];
            phoneNum1.Text = b[8];
            phoneNum2.Text = b[9];
            email.Text = b[10];



        }

        private void reset_Click(object sender, RoutedEventArgs e)
        {
            idNum.Text = "";
            fName.Text = "";
            lName.Text = "";
            DOB.Text = "";
            strNum.Text = "";
            strName.Text = "";
            postCode.Text = "";
            cityName.Text = "";
            phoneNum1.Text = "";
            phoneNum2.Text = "";
            email.Text = "";
        }

        private void refreshBtn_Click(object sender, RoutedEventArgs e)
        {
            listOverlay.ItemsSource = loadAllTheNumbers();
        }

        static List<string> loadAllTheNumbers()
        {
            //Set the command and executes it and returns a list
            var idNumber = $"Select ID, FNAME from tbl_people";

            //Call the custom method
            var idNumbers = MySQLCustom.ShowInList(idNumber);

            //Display the list (in this case a combo list)
            return idNumbers;
        }
        static List<string> clearList()
        {

            var clear = "";

            var listClear = MySQLCustom.ShowInList(clear);

            return listClear;
        }

        private void refreshBtnPivot_Click(object sender, RoutedEventArgs e)
        {

        }
    }
    
}
